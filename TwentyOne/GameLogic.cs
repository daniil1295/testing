﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{
    
    
    class GameLogic
    {
        private DeckDealer _deckDealer;

        private Player _user;

        private Player _computer;

        private GameState _gameState;

        public GameLogic(string playerName)
        {
            _deckDealer = new DeckDealer();
            _user = new Player(playerName);
            _computer = new Player("Computer");
            _gameState = GameState.Playing;
        }

        public void SetPlayersFirstTwoCards()
        {
            PlayerLogic.AddCard(_deckDealer.GetCard, _user);
            PlayerLogic.AddCard(_deckDealer.GetCard, _user);

            PlayerLogic.AddCard(_deckDealer.GetCard, _computer);
            PlayerLogic.AddCard(_deckDealer.GetCard, _computer);

            FindWinnerByPlayerState();
        }

        public string GetUsersCards()
        {
            return _user.ToString();
        }

        public string GetComputersCards()
        {
            return _computer.ToString();
        }

        public bool GiveUserNewCard()
        {
            PlayerLogic.AddCard(_deckDealer.GetCard, _user);
            FindWinnerByPlayerState();
            return IsGamePlaying();
        }

        public bool GiveComputerNewCard()
        {
            PlayerLogic.AddCard(_deckDealer.GetCard, _computer);
            FindWinnerByPlayerState();
            return IsGamePlaying();
        }

        public string GetUsersName()
        {
            return _user.PlayerName;
        }

        public void FindWinnerByPoints()
        {
            
            if (PlayerLogic.GetPlayerScore(_computer) == PlayerLogic.GetPlayerScore(_user))
            {
                _gameState = GameState.Draw;
            }
            if (PlayerLogic.GetPlayerScore(_computer) > PlayerLogic.GetPlayerScore(_user))
            {
                _gameState = GameState.ComputerWon;
            }
            if (PlayerLogic.GetPlayerScore(_computer) < PlayerLogic.GetPlayerScore(_user))
            {
                _gameState = GameState.UserWon;
            }
        }

        public void FindWinnerByPlayerState()
        {
            _gameState = GameState.Playing;
            if (_user.PlayerState == PlayerState.Winner || _computer.PlayerState == PlayerState.Looser)
            {
                _gameState = GameState.UserWon;
            }
            if (_computer.PlayerState == PlayerState.Winner || _user.PlayerState == PlayerState.Looser)
            {
                _gameState = GameState.ComputerWon;
            }
            if (_user.PlayerState == PlayerState.Winner && _computer.PlayerState == PlayerState.Winner)
            {
                _gameState = GameState.Draw;
            }
        }

        public bool IsGamePlaying()
        {
            return _gameState == GameState.Playing;
        }

        public GameState GetGameState()
        {
            return _gameState;
        }
    }
}
