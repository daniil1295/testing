﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{
    class DeckDealer
    {
        private Deck _deck;

        public DeckDealer()
        {
            CreateNewDeck();
        }

        public Card GetCard
        {
            get
            {
                var card = _deck.CardDeck.Last();
                _deck.CardDeck.Remove(card);
                return card;
            }
        }

        private void CreateNewDeck()
        {
            _deck = new Deck();
            FillDeck();
            ShuffleDeck();
        }

        private void FillDeck()
        {
            for (var suit = (int)Suit.Spades; suit <= (int)Suit.Clubs; suit++)
            {
                FillDeckFaces((Suit)suit);
            }
        }

        private void FillDeckFaces(Suit suit)
        {
            for (var face = (int)Face.Two; face <= (int)Face.Ace; face++)
            {
                FillDeckValues(suit, (Face)face);                
            }
        }

        private void FillDeckValues(Suit suit, Face face)
        {
            var jackValue = 2;
            var queenValue = 3;
            var kingValue = 4;
            var aceValue = 11;
            if (face < Face.Jack)
            {
                _deck.CardDeck.Add(new Card()
                {
                    CardFace = face,
                    CardSuit = suit,
                    CardValue = (int)face
                });
                return;
            }

            var cardValue = 0;
            if (face == Face.Jack)
            {
                cardValue = jackValue;
            }
            if (face == Face.Queen)
            {
                cardValue = queenValue;
            }
            if (face == Face.King)
            {
                cardValue = kingValue;
            }
            if (face == Face.Ace)
            {
                cardValue = aceValue;
            }
            _deck.CardDeck.Add(new Card()
            {
                CardFace = face,
                CardSuit = suit,
                CardValue = cardValue
            });
        }

        private void ShuffleDeck()
        {
            var card = new Card();
            for (var i = 0; i < _deck.CardDeck.Count / 2; i++)
            {
                var randomValueFirst = Randomaiser.GetRandom(_deck.CardDeck.Count);
                var randomValueSecond = Randomaiser.GetRandom(_deck.CardDeck.Count);
                card = _deck.CardDeck[randomValueFirst];
                _deck.CardDeck[randomValueFirst] = _deck.CardDeck[randomValueSecond];
                _deck.CardDeck[randomValueSecond] = card;
            }
        }
    }
}
