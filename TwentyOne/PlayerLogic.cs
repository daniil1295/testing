﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{

    public class PlayerLogic
    {

        private static WinnerFounder _winnerFounder = new WinnerFounder();

        public static void AddCard(Card card, Player player)
        {
            player.CardsList.Add(card);
            if (player.CardsList.Count == 2)
            {
                CheckGoldenPoint(player);
                return;
            }
            UpdatePlayerState(player);
        }

        public static void CheckGoldenPoint(Player player)
        {
            player.PlayerState = _winnerFounder.CheckGoldenPoint(player.CardsList);
        }

        public static void UpdatePlayerState(Player player)
        {
            player.PlayerState = _winnerFounder.FindWinner(GetPlayerScore(player));
        }

        public static int GetPlayerScore(Player player)
        {
            var playerScore = 0;
            foreach (Card card in player.CardsList)
            {
                playerScore += card.CardValue;
            }
            return playerScore;
        }
    }
}
