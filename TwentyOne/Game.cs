﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

namespace TwentyOne
{
    public class Game
    {
        private GameLogic _gameLogic;

        private Speaker _speaker;

        private const int _maxCountOfLoopsForCardIssuance = 3;

        public Game()
        {
            this._speaker = new Speaker();
            StartGame();
        }

        private void StartGame()
        {
            
            _gameLogic = new GameLogic(_speaker.AskTheName());
            _speaker.Greeting(_gameLogic.GetUsersName());
            _gameLogic.SetPlayersFirstTwoCards();
            _speaker.ShowCards(_gameLogic.GetUsersCards());

            if (_gameLogic.IsGamePlaying())
            {
                StartLoop();
            }

            _speaker.ShowWinner(_gameLogic.GetGameState());
            _speaker.ShowCards(_gameLogic.GetComputersCards());
            
            if (_speaker.IsStartsNewGame())
            {
                _speaker.ClearConsole();
                StartGame();
            }
            _speaker.Thanks();
        }

        private void StartLoop()
        {
            var isPlaying = true;
            for (var i = 0; i < _maxCountOfLoopsForCardIssuance; i++)
            {
                if (!_speaker.IsTakesNewCard())
                {
                    _gameLogic.FindWinnerByPoints();
                    break;
                }

                isPlaying = _gameLogic.GiveUserNewCard();
                _speaker.ShowCards(_gameLogic.GetUsersCards());

                if (!isPlaying)
                {
                    break;
                }
                if (!_gameLogic.GiveComputerNewCard())
                {
                    break;
                }  
            }
            if (_gameLogic.IsGamePlaying())
            {
                _gameLogic.FindWinnerByPoints();
            }
        }
    }
}
