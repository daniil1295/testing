﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{
    public class Speaker
    {
        public void Greeting(string playerName)
        {
            Console.WriteLine("Hello, " + playerName + ", good luck!");
        }

        public void ShowCards(string cards)
        {
            Console.WriteLine(cards);
        }

        public bool IsTakesNewCard()
        {
            Console.WriteLine("\nWanna take one more card? 1 - yes, 0 - no");
            return Console.ReadLine() == "1";
        }

        public bool IsStartsNewGame()
        {
            Console.WriteLine("\nWanna play again ? 1 - yes, 0 - no");
            return Console.ReadLine() == "1";
        }

        public void Thanks()
        {
            Console.WriteLine("Thank you for the game!");
        }

        public string AskTheName()
        {
            Console.WriteLine("Please, enter your name: ");
            return Console.ReadLine();
        }

        public void ClearConsole()
        {
            Console.Clear();
        }

        public void ShowWinner(GameState gameState)
        {
            if(gameState== GameState.ComputerWon)
            {
                Console.WriteLine("\nYou lose :c\n");
            }
            if (gameState == GameState.UserWon)
            {
                Console.WriteLine("\nYou won!\n");
            }
            if (gameState == GameState.Draw)
            {
                Console.WriteLine("\nDraw\n");
            }
        }
    }
}
