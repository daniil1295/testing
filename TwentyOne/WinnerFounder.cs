﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{
    public class WinnerFounder
    {

        private const int _pointsForWin = 21;

        public PlayerState FindWinner(int playerScore)
        {
            if (playerScore == _pointsForWin)
            {
                return PlayerState.Winner;
            }
            if (playerScore > _pointsForWin)
            {
                return PlayerState.Looser;
            }
            return PlayerState.InGame;
        }
        public PlayerState CheckGoldenPoint(List<Card> cardsList)
        {
            var firstCardIndex = 0;
            var secondCardIndex = 1;
            if (cardsList[firstCardIndex].CardFace == Face.Ace && cardsList[secondCardIndex].CardFace == Face.Ace
                || cardsList[firstCardIndex].CardValue + cardsList[secondCardIndex].CardValue == _pointsForWin)
            {
                return PlayerState.Winner;
            }
            return PlayerState.InGame;
        }
    }
}
