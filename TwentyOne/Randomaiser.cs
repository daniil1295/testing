﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwentyOne
{
    public class Randomaiser
    {
        public static Random Random = new Random(DateTime.Now.Millisecond);

        public static int GetRandom(int value)
        {
            return Random.Next(value);
        }
    }
}
