﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwentyOne.Models
{
    public class Player
    {
        public PlayerState PlayerState { get; set; }

        public string PlayerName { get; }

        public List<Card> CardsList { get; set; }

        public Player(string playerName)
        {
            PlayerState = PlayerState.InGame;
            CardsList = new List<Card>();
            this.PlayerName = playerName;
        }

        public override string ToString()
        {
            var result = PlayerName + "'s cards\n";
            var allPoints = 0;
            foreach (Card card in CardsList)
            {
                result += card.CardFace + " " + card.CardSuit + " Points: " + card.CardValue + "\n";
                allPoints += card.CardValue;
            }
            result += "Score: " + allPoints;
            return result;
        }
    }
}
