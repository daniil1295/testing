﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwentyOne.Models
{
    public enum GameState
    {
        Draw,
        ComputerWon,
        UserWon,
        Playing
    }
}
