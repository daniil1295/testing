﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwentyOne.Models;

namespace TwentyOne
{
    public class Card
    {
        public Face CardFace { get; set; }
        public Suit CardSuit { get; set; }
        public int CardValue { get; set; }
    }
}
